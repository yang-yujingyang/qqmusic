const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  // webpack-dev-server 相关配置
devServer: {
  proxy: {
      '/api': {
          target: 'http://171.220.242.237:3002',
          ws: true,
          changeOrigin: true,
          pathRewrite: {
              '^/api': ''  //通过pathRewrite重写地址，将前缀/api转为/
          }
      }
  }
},
})


