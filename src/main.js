import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { Tabbar, TabbarItem } from 'vant';
import {Calendar} from 'vant';
import { Cell } from 'vant'; 
import { Search } from 'vant';
import { Swipe, SwipeItem } from 'vant';
import request from './common/request.js';
import { Image as VanImage,Lazyload } from 'vant';
import { NavBar } from 'vant';
import { List } from 'vant';

Vue.use(List);
Vue.use(NavBar);
Vue.use(VanImage);
Vue.use(Lazyload);
Vue.prototype.$request=request.httpRequest
Vue.use(Swipe);
Vue.use(SwipeItem);
Vue.use(Search);
Vue.use(Tabbar);
Vue.use(TabbarItem);
Vue.use(Calendar);
Vue.use(Cell);
Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')


