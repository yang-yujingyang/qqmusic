import BScroll from "better-scroll";

// 横向滚动方法
/**
 * 
 * @param {*} that 代表实列（vue组件）
 * @param {*} wrapper 代表滚动的容器
 * @param {*} content 代表滚动的内容区
 * @param {*} num 代表滚动项的数目
 * @param {*} itemWidth 代表滚动项的宽度rem
 */
// function betterScrollHorizontal=(num,itemWidth)
let betterScrollHorizontal = function(that,wrapper,content,num,itemWidth){
    content.style.width = num * itemWidth+"rem";
    that.$nextTick(()=>{
        //wrapper为template中容器的ref,表示给那个元素来设置滚动
        that.scroll = new  BScroll(wrapper,{
            startX:0,//横向滚动的开始位置
            scrollX:true,//水平横向滚动
            scrollY:false,//取消纵向滚动
            click:true//开启浏览器的原生click事件
        })
    })
}
// 输出
export {betterScrollHorizontal};