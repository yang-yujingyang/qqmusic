// 引入axios
import axios from 'axios'

/**
 * 输出 default，有一个httpRequest方法，用于发送http请求
 * @param methods ,请求方法：get或post或put
 * @param url  ,请求地址
 * @param data ,请求数据
 * baseURL,基础的url地址，会加载在url前面，组成最终的服务器请求地址，
 * 本处写的是vue.config.js中的代理地址，实际上这个值就是http://171.220.242.237:3002
 * timeout,指定请求超时的毫秒数
 * headers,被发送的自定义请求头
 */
export default{
  httpRequest(methods,url,data){
    return axios({
      methods,
      url,
      data,
      baseURL: '/api',
      timeout:10000,
      headers:{
        'content-type': 'application/json;charset=utf-8'
      }
    }).then(response=>{
      return response.data;
    })
  }
}